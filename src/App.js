
import './App.css';
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Header from './components/Layout/Header'
import Home from './pages/Home';
import About from './pages/About';
import Menu from './pages/Menu';
import Register from './pages/Register';
import Login from './pages/Login';


function App() {
  return (
    <div>
      <BrowserRouter>
      <Header/>
      <div className='main-content'>
      <Routes>
        <Route path="/" element={<Home />}/>
        <Route path="/about"  element={<About/>}/>
        <Route path="/menu" element={<Menu/>}/>
        <Route path='/register'  element={<Register/>}/>
        <Route path='/login'  element={<Login />}/>
      </Routes>
      </div>
      </BrowserRouter>
    </div>
  );
}

export default App;

