import React from 'react';
import Layout from '../components/Layout/Layout';
import '../styles/About.css'
const About = () => {
  return (
    <Layout>
      <center>
      <div className='d-flex flex-column justify-content-center w-100 h-100'>
        <div>
          <h1>About us</h1>
        </div>
        <div>
            <h3>Icekart Icecreams is probably the most well known Hatsun brand. When other ice cream brands<br/> opened parlours exclusively in the city, Icekart decided to take to suburban and even rural<br/>  areas, leading the ice cream into the hearts of millions. Icekart caters to different people <br/> with different tastes with a whole range of ice cream bars exclusively for kids and <br/> novel ice cream flavours like indian sweets. The brand also consistently introduces new <br/> flavours every season, just to make sure customers have something fresh to look<br/>  forward to every time they walk in to an Icekart Icecreams parlour.</h3>
        </div>
        <div>
          <img src='https://st.depositphotos.com/1669785/4792/i/380/depositphotos_47926331-stock-photo-strawberry-fruit-ice-cream.jpg' id='aboutimg'></img>
        </div>
      </div></center>
      
    </Layout>
  );
};

export default About;

