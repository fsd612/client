
import axios from 'axios'
import React, { useEffect, useState } from 'react';
import Layout from '../components/Layout/Layout';
import { Card, CardBody, CardTitle, Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label } from 'reactstrap';
import Rating from 'react-rating-stars-component';

import '../styles/menu.css';

function Menu() {
  const [name, setName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [address, setAddress] = useState('');
  const [number_of_scoops, setNumber] = useState('');
  const [modal, setModal] = useState(false);
  const [data, setData] = useState([])


  const toggle = () => setModal(!modal);


  const handleOrderClick = () => {
    toggle();
  };

  useEffect(() => {
    axios.get('https://server-nkcu.onrender.com/getProductData')
      .then(res => setData(res.data))
      .catch(err => console.log(err))
  }, [])

  console.log(data)

  const handleConfirmOrder = async () => {

    if (!name || !mobileNumber || !address || !number_of_scoops) {
      alert('Please fill in all required fields.');
      return;
    }
    try {
      const userDetails = {
        name,
        mobileNumber,
        address,
        number_of_scoops: number_of_scoops,
      };

      const response = await axios.post('https://server-nkcu.onrender.com/data', userDetails);

      if (response.status === 200) {
        window.alert('Order placed successfully');
        toggle();
      } else {
        window.alert('Failed to place order. Please try again.');
      }
    } catch (error) {
      console.error('Error while placing order:', error);
      window.alert('Error while placing order. Please try again.');
    }
  };

  return (
    <Layout>
      <div style={{backgroundColor:"papayawhip"}} className='w-100 d-flex justify-content-center gap-3 flex-wrap'>
        {
          data.map((item, index) => (
            <Card className=' bg-transparent' key={index} style={{ width: '18rem' }}>
              <img alt="Sample" src={item.productImg} />
              <CardBody>
                <CardTitle tag="h5">
                  <h3 className='title'>{item.productName}</h3>
                </CardTitle>
                <h3 className='price'>price ${item.productPrice}</h3>
                <div className=' d-flex flex-column align-items-center justify-content-center'>
                  <Rating
                    value={item.productRating}
                    size={24}
                    isHalf={true}
                    activeColor="#ffd700"
                  />
                  <Button style={{backgroundColor:'blueviolet'}} onClick={handleOrderClick}>
                    Order
                  </Button>

                </div>
              </CardBody>
            </Card>
          ))
        }

        <div className='modmain'>

          {/* Modal for entering details */}

          <Modal isOpen={modal} toggle={toggle} centered>
            <ModalHeader className='modalheader' toggle={toggle}>ENTER DETAILS</ModalHeader>
            <ModalBody className='modalbody'>
              <center> <Label className='labelname' for="name">Name</Label>

                <Input className='ipone'
                  type="text"
                  id="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  placeholder="Enter your name"
                /> <br></br></center>

              <center>
                <Label className='labelname' for="mobileNumber">Mobile Number</Label>
                <Input className='iptwo'
                  type="text"
                  id="mobileNumber"
                  value={mobileNumber}
                  onChange={(e) => setMobileNumber(e.target.value)}
                  placeholder="Enter your mobile number"
                /><br></br>

              </center>

                <Label className='labelname' for="address">Address</Label>
                <Input className='ipthree'
                  type="text"
                  id="address"
                  value={address}
                  onChange={(e) => setAddress(e.target.value)}
                  placeholder="Enter your address"
                />

              <center>
                <Label className='labelname' for="number">Number of scoops</Label>
                <Input className='ipfour'
                  type="text"
                  id="number"
                  value={(number_of_scoops)}
                  onChange={(e) => setNumber(e.target.value)}
                  placeholder="Enter number of scoops"
                />
              </center>

            </ModalBody>
            <ModalFooter>
                <Button className='conbtn' color="primary" onClick={handleConfirmOrder}>
                 Confirm
                </Button> {' '}
                <Button className='conbtn' color="secondary" onClick={toggle}>
                  Cancel
                </Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    </Layout>
  );
}

export default Menu;
