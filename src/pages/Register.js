
import React, { useState } from 'react';
import { Container, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../styles/Register.css';
import Layout from '../components/Layout/Layout';


const Register = () => {
  const [data, setData] = useState({ Name: '', Email: '', Password: '', MobileNo: '', DOB: '', Country: '' });


  const registeredUser = async (e) => {
    e.preventDefault();
    const errors = {};
  
    // Check for empty email field
    if (!data.Email) {
      errors.email = 'Email is Mandatory';
    }
  
    // Password validation
    const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+])(?=.{8,})/;
    if (!passwordPattern.test(data.Password)) {
      errors.password = 'Password should be at least 8 characters. It should have at least one special character. It must contain one uppercase letter';
    }
  
    // Mobile number validation
    const mobilePattern = /^\d{10}$/;  // Regex pattern to match exactly 10 digits
    if (!mobilePattern.test(data.MobileNo)) {
      errors.mobileNo = 'Mobile number should be exactly 10 digits.';
    }
  
    // Check if there are any errors
    if (Object.keys(errors).length > 0) {
      let errorMessage = 'Please fill in all required fields.\n';
      for (const key in errors) {
        errorMessage += `${errors[key]}\n`;
      }
      alert(errorMessage);
      return;
    }
  
    try {
      const response = await axios.post('https://server-nkcu.onrender.com/register', data);
      if (response.status === 200) {
        alert('Registration Successful!');
        localStorage.setItem('token', response.data.token);
        setData({ Name: '', Email: '', Password: '', MobileNo: '', DOB: '', Country: '' });
   //     setAuthenticated(true);
        window.location.href = '/';
      } else {
        alert('Registration Failed, please try again');
      }
    } catch (error) {
      console.error('Error submitting form', error);
      alert('Registration Failed, please try again');
    }
  };
    
  const ContainerStyle = {
    backgroundColor: 'white',
      height: '690px',
      width:'400px',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor:'gray'
  };

  return (
    <Layout>
    <div className='register'>
      <Container className=" mb-10  p-7 rounded  container mx-md-4" style={ContainerStyle }>
        <center>
          <h5 className="text-center mb-2 mt-4" style={{ color: 'black', fontWeight: 'bold', fontSize: '20px', marginBottom: '20px', marginTop: '20px' }}>Register</h5>
        </center>

        <Form onSubmit={registeredUser}>
          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="name">Name</Label>
            <Input style={{marginRight:'100px'}}
              type="text"
              placeholder="Enter your name"
              value={data.Name}
              onChange={(e) => setData({ ...data, Name: e.target.value })} />
          </FormGroup>

          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="email">Email</Label>
            <Input style={{ }}
              type="email"
              placeholder="Enter your email"
              value={data.Email}
              onChange={(e) => setData({ ...data, Email: e.target.value })} />
          </FormGroup>

          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="password">Password</Label>
            <Input style={{ }}
              type="password"
              placeholder="Enter your password"
              value={data.Password}
              onChange={(e) => setData({ ...data, Password: e.target.value })} />
          </FormGroup>

          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="mobileNo">MobileNo</Label>
            <Input style={{  }}
              type="text"
              placeholder="Enter your mobile number"
              value={data.MobileNo}
              onChange={(e) => setData({ ...data, MobileNo: e.target.value })} />
          </FormGroup>

          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="dob">DOB</Label>
            <Input style={{ }}
              type="date"
              placeholder="Enter your date of birth"
              value={data.DOB}
              onChange={(e) => setData({ ...data, DOB: e.target.value })} />
          </FormGroup>

          <FormGroup className="mb-3">
            <Label style={{ paddingLeft: '10px' }} for="country">Country</Label>
            <Input style={{  }}
              type="text"
              placeholder="Enter your country"
              value={data.Country}
              onChange={(e) => setData({ ...data, Country: e.target.value })} />
          </FormGroup>

          <center>
            <Button type="submit" style={{ backgroundColor: 'green', color: "#fff", paddingLeft: '110px', paddingRight: "110px", borderRadius: '8px', marginBottom: "30px", paddingTop: '10px', paddingBottom: '10px', marginTop: '20px', border:'none'}}>
              Register
            </Button>
          </center>
          <p style={{ paddingLeft: '10px' }} className='text-dark mt-2'>
            Already have an account? <Link to="/login" style={{ color: 'blue' }}>Login</Link>
          </p>
        
        </Form>
      </Container>
    </div>
    </Layout>
  );
};

export default Register;


