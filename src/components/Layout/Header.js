
import React, { useState } from 'react';
import { AppBar, Box, Drawer, IconButton, Toolbar, Typography, Divider } from '@mui/material';
import IcecreamIcon from '@mui/icons-material/Icecream';
import { Link,useNavigate} from 'react-router-dom';
import MenuIcon from '@mui/icons-material/Menu';
import '../../styles/HeaderStyle.css';

const Header = () => {
  const [mobileOpen, setMobileOpen] = useState(false);
  const navigate = useNavigate();
  const auth = localStorage.getItem('token')
 // const isAuthenticated = localStorage.getItem('token')


  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const logout = () =>{
   
    const confirmLogout = window.confirm('Do you want to logout?');
    if (confirmLogout) {
      localStorage.removeItem('token');

      navigate('/');
    }
  }

  const drawer = (
    <Box sx={{ textAlign: 'center' }}>
      <Typography color="goldenrod" variant="h6" component="div" sx={{ flexGrow: 1, my: 2 }}>
        <h1>
          <IcecreamIcon /> Icekart
        </h1>
      </Typography>
      <Divider />

      <ul className="mobile-navigation">
        <li>
          <Link to={'/'} onClick={handleDrawerToggle}>
            Home
          </Link>
        </li>
        <li>
          <Link to={'/about'} onClick={handleDrawerToggle}>
            About
          </Link>
        </li>
        <li>
          <Link to={'/menu'} onClick={handleDrawerToggle}>
            Menu
          </Link>
        </li>

       <li>
          <Link to={'/login'} onClick={handleDrawerToggle}>
            Login
          </Link>
        </li>    
        <li>
          <Link to={'/register'} onClick={handleDrawerToggle}>
            Register
          </Link>
        </li>     
      </ul>
    </Box>
  );
  return (
    <>
      <Box>
        <AppBar component="nav" sx={{ backgroundColor: 'white' }}>
          <Toolbar>
            <IconButton
              color="black"
              aria-label="open drawer"
              edge="start"
              sx={{ mr: 2, display: { sm: 'none' } }}
              onClick={handleDrawerToggle}
            >
              <MenuIcon />
            </IconButton>
            <Typography color="blue" variant="h6" component="div" sx={{ flexGrow: 1 }}>
              <h1 style={{fontFamily:"fantasy"}}>
                <IcecreamIcon style={{fontSize:'40px'}} /> Icekart
              </h1>
            </Typography>
            
            {auth ? ( 

            <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
              <ul className="navigation-menu">
                <li>
                  <Link to={'/'}>Home</Link>
                </li>
                <li>
                  <Link to={'/about'}>About </Link>
                </li>
                <li>
                  <Link to={'/menu'}>Menu</Link>
                </li>              
                <li>
                    <button className='logoutbtn' onClick={logout}>Logout</button>
                  </li>
              </ul>
            </Box>

            ) : (

              <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
              <ul className="navigation-menu">
                <li>
                  <Link to={'/'}>Home</Link>
                </li>
                <li>
                  <Link to={'/about'}>About </Link>
                </li>                
                <li>
                  <Link to={'/login'}>Login</Link>
                </li>
                <li>
                  <Link to={'/register'}>Register</Link>
                </li>
              </ul>
            </Box>
          )}

          </Toolbar>
        </AppBar>
        <Box component="nav">
          <Drawer
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            sx={{
              display: { xs: 'block', sm: 'none' },
              '& .MuiDrawer-paper': { boxSizing: 'border-box', width: '240px' },
            }}>
            {drawer}
          </Drawer>
        </Box>
        <Toolbar />
      </Box>
    </>
  );
};

export default Header;




