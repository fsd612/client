import React from 'react';
import { Typography, Box } from '@mui/material';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import { fontSize } from '@mui/system';
import "../../styles/Footer.css"

function Footer() {
  return (
    <>
      <Box sx={{ textAlign: 'center', bgcolor: 'black', color: 'white', p: 3 }}>
        <Typography
          variant="h5"
          sx={{
            '@media (max-width:600px)': { // Corrected typo here
              fontSize: '1rem',
            },
          }}
        >
          <div className='footer'>
            <div>
              <ul >
                <li><h1><a>Legal</a></h1></li>
                <li><a href=''>Cookie notice</a></li>
                <li><a href=''>Accessibility</a></li>
                <li><a href=''>Legal notice</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><h1>Help</h1></li>
                <li><a href=''>FAQ</a></li>
                <li><a>Contact us</a></li>
                <li><a>Sitemap</a></li>
                <li><a>Payments</a></li>
              </ul>
            </div>
            <div>
              <ul>
                <li><h1>CONSUMER POLICY</h1></li>
                <li><a>Security</a></li>
                <li><a>Privacy</a></li>
                <li><a>EPR Complience</a></li>
                <li><a>Terms of use</a></li>

              </ul>
            </div>
            <div>
              <ul>
                <li><h1>Mail Us</h1></li>
                <li>Ice cream pvt ltd, Hyderabad, Telangana</li>
                
              </ul>
            </div>
            
          </div>
          <Box
            sx={{
              my: 3,
              '& svg': {
                fontSize: '60px',
                cursor: 'pointer', 
                mr: 2,
                '&:hover': { 
                  transform: 'translateX(5px)',
                  transition: 'all 400ms',
                },
              },
            }}
          >
            <InstagramIcon className='insta'/>
            <FacebookIcon  className='fb' />
            <TwitterIcon className='twt' />
          </Box>
        </Typography>
      </Box>
    </>
  );
}

export default Footer;